#!/usr/bin/env python
import math

def getFrac(n):
    
    frac, whole = math.modf(n)
    frac = int(frac * 10)
    
    return frac
