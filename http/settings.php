<?php

  require_once("config.php");
  require_once("plcModel.php");
	require_once("header.php");

  $m = new PlcModel();

  if(isset($_POST['gsettings'])){

  	$plc_read = intval($_POST['plc_read_interval']);
  	$api_send = intval($_POST['api_send_interval']);

  	if($plc_read > 3500) $plc_read = 3500;
  	if($api_send > 3500) $api_send = 3500;

    $m->insertSetting($_POST['plc_read_interval_id'], $plc_read);
    $m->insertSetting($_POST['api_send_interval_id'], $api_send);
    $m->insertSetting($_POST['admin_email_id'], $_POST['admin_email']);

    file_put_contents('/var/www/html/plc', '');

    if($plc_read < 60){
    	
    	$cmd1 = '/usr/bin/python3 /home/pi/plc/plc_master.py '.$plc_read.' > /var/www/html/plc 2>&1 &'.PHP_EOL;
    	file_put_contents('/home/pi/plc/runjobs',  "ps aux  |  grep -i plc_master.py  |  awk '{print $2}'  | xargs kill -9 > /dev/null 2>&1 ".PHP_EOL);
    	file_put_contents('/home/pi/plc/runjobs',  $cmd1 , FILE_APPEND);
    	file_put_contents('/home/pi/plc/jobsinprogress',  $cmd1);
    	file_put_contents('/home/pi/plc/crontab.txt',  '#'.PHP_EOL);

    }else{
    	$plc_read = intval($plc_read / 60);
    	$cmd1 = '*/'.$plc_read.' * * * * /usr/bin/python3 /home/pi/plc/plc_master.py > /var/www/html/plc 2>&1 &'.PHP_EOL;
    	unlink('/home/pi/plc/jobsinprogress');
    	file_put_contents('/home/pi/plc/runjobs',  "ps aux  |  grep -i plc_master.py  |  awk '{print $2}'  | xargs kill -9 > /dev/null 2>&1 ".PHP_EOL);
    	file_put_contents('/home/pi/plc/crontab.txt',  $cmd1);
    }

    if($api_send < 60){
    	
    	$cmd2 = '/usr/bin/php /var/www/html/restUploader.php '.$api_send.' >> /var/www/html/plc 2>&1 &'.PHP_EOL;
    	file_put_contents('/home/pi/plc/runjobs',  "ps aux  |  grep -i restUploader.php  |  awk '{print $2}'  | xargs kill -9 > /dev/null 2>&1 ".PHP_EOL , FILE_APPEND);
    	file_put_contents('/home/pi/plc/runjobs',  $cmd2, FILE_APPEND);
    	file_put_contents('/home/pi/plc/jobsinprogress2',  $cmd2);
    	file_put_contents('/home/pi/plc/crontab.txt',  '#'.PHP_EOL);

    }else{
    	$api_send = intval($api_send / 60);
    	$cmd2 = '*/'.$api_send.' * * * * /usr/bin/php /var/www/html/restUploader.php >> /var/www/html/plc 2>&1'.PHP_EOL;
    	unlink('/home/pi/plc/jobsinprogress2');
    	file_put_contents('/home/pi/plc/runjobs',  "ps aux  |  grep -i restUploader.php  |  awk '{print $2}'  | xargs kill -9 > /dev/null 2>&1 ".PHP_EOL , FILE_APPEND);
    	file_put_contents('/home/pi/plc/crontab.txt',  $cmd2, FILE_APPEND);
    }
    
	file_put_contents('/etc/postfix/virtual',  'pi@localhost '.$_POST['admin_email']);

    $updated = "Settings updated.";

  }

  $rows = $m->getAllSettings();

?>

<style>
  label {
    display: inline-block;
    margin-bottom: .5rem;
    padding-left: 2px;
  }

  h2 {
    font-weight: bold;
  }

  hr {
    margin-top: 135px;
  }
  .row{
    background-color: aliceblue;
    padding: 3px;
    margin-top: 5px;
  }
</style>

<div class="container">
  	<div class="row">
      <div class="col-sm-12">
      	<h2 style="text-align: center;">General Settings</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
      	<h4 style="text-align: center;"><?php echo $updated;?></h4>
      </div>
    </div>
    <?php
      if($rows){
        $r=$rows[0];

    ?>
<form method="post" action="">
    <div class="row">
      <div class="col-md-3">
         <label>PLC Read Interval (seconds)</label>
         <input type="hidden" name="plc_read_interval_id" value="<?php echo $r['id'];?>" />
      </div>
      <div class="col-md-3">
         <div class="form-group">
		  <input type="text" class="form-control" name="plc_read_interval" value="<?php echo $r['svalue'];?>" pattern="^\d+$" title="Seconds only" required >
		</div>
      </div>
      <div class="col-md-6"></div>
    </div>

    <?php $r=$rows[1];?>

    <div class="row">
      <div class="col-md-3">
         <label>API Send Interval (seconds)</label>
         <input type="hidden" name="api_send_interval_id" value="<?php echo $r['id'];?>" />
      </div>
      <div class="col-md-3">
         <div class="form-group">
		  <input type="text" class="form-control" name="api_send_interval" value="<?php echo $r['svalue'];?>" pattern="^\d+$" title="Seconds only" required >
		</div>
      </div>
      <div class="col-md-6"></div>
    </div>

    <?php $r=$rows[2];?>
    <div class="row">
      <div class="col-md-3">
         <label>Admin Email</label>
         <input type="hidden" name="admin_email_id" value="<?php echo $r['id'];?>" />
      </div>
      <div class="col-md-3">
         <div class="form-group">
		  <input type="text" class="form-control" name="admin_email" value="<?php echo $r['svalue'];?>" required >
		</div>
      </div>
      <div class="col-md-6"></div>
    </div>

    <div class="row">
      <div class="col-md-3">
        
      </div>
      <div class="col-md-3">
         <div class="form-group">
         	<button type="submit" name="gsettings" value='yes' class="btn btn-primary">Update Settings</button>
		</div>
      </div>
      <div class="col-md-6"></div>
    </div>
</form>
   
    <?php
        
      }//if rows
    ?>

<?php require_once 'footer.php';?>
