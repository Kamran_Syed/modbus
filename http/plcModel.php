<?php

if(!class_exists('PlcModel')){
	class PlcModel{
				
		function __construct(){

		}

		function getUploadData(){
			global $db;

			$sql = $db->prepare("SELECT d.*, m.PLC_id, m.Tag_Name FROM PLC_Data as d, Register_Map as m where d.Tx_Time = 0 AND d.Map_id=m.id order by m.PLC_id, d.Rx_Time ");
			$sql->execute();
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $r; //will be false if not found

		}

		function log($msg){
			global $db;

			$sql = $db->prepare("INSERT INTO PLC_logs SET dt=? , logType=? , msg=? ");
			$sql->execute(array(time(), 'web', $msg ));

		}

		function getLog(){
			global $db;

			$sql = $db->prepare("SELECT * FROM PLC_logs order by dt desc limit 100");
			$sql->execute();
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			
			return $r; //will be false if not found

		}

		function getPlcConfig($plcId, $key){
			global $db;

			$sql = $db->prepare("SELECT * FROM ConfigurationDetail Where PLC_id=? AND `key`=? ");
			$sql->execute(array($plcId, $key ));
			$r = $sql->fetch(PDO::FETCH_ASSOC);
			
			return $r['value']; //will be false if not found

		}

		function updateTx($dataIds){
			global $db;

			$idlst = implode(',', $dataIds);

			$sql = $db->prepare("UPDATE PLC_Data SET Tx_Time=? WHERE id IN (".$idlst.")");
			$sql->execute(array(time()));

		}

		function updateConfig($kv){
			global $db;

			$sql = $db->prepare("Delete FROM ConfigurationDetail where PLC_id=? ");
			$sql->execute(array($kv['plc_id']));

			$this->insertConfig($kv['plc_id'], 'plc_ipaddress', $kv['plc_ipaddress']);
			$this->insertConfig($kv['plc_id'], 'plc_port', $kv['plc_port']);
			$this->insertConfig($kv['plc_id'], 'api_endpoint', $kv['api_endpoint']);
			$this->insertConfig($kv['plc_id'], 'api_authkey', $kv['api_authkey']);

		}

		function insertConfig($plc_id, $k, $v){
			global $db;

			$sql = $db->prepare("INSERT into ConfigurationDetail SET PLC_id=? , `key` =? ,  value =? ");
			$sql->execute(array($plc_id, $k, $v));

		}

		function getAllPlc(){
			global $db;

			$sql = $db->prepare("SELECT * FROM PLC_Configuration order by name ");
			$sql->execute();
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			
			return $r; //will be false if not found

		}

		function getAllSettings(){
			global $db;

			$sql = $db->prepare("SELECT * FROM GenSettings ");
			$sql->execute();
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			
			return $r; //will be false if not found

		}

		function getRegisterMap(){
			global $db;

			$sql = $db->prepare("SELECT * FROM Register_Map ");
			$sql->execute();
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			
			return $r; //will be false if not found

		}

		function insertSetting($id, $v){
			global $db;

			$sql = $db->prepare("UPDATE GenSettings SET svalue =? where id=? ");
			$sql->execute(array($v, $id));

		}

		function getPlc($plcId){
			global $db;

			$sql = $db->prepare("SELECT * FROM PLC_Configuration where id=? ");
			$sql->execute(array($plcId));
			$r = $sql->fetch(PDO::FETCH_ASSOC);
			
			return $r; //will be false if not found

		}

		function createNewPlc($newName, $newDesc){
			global $db;

			$sql = $db->prepare("INSERT into PLC_Configuration SET name=?, description=? ");
			$sql->execute(array($newName, $newDesc));

			return $db->lastInsertId(); 

		}

		function delete($plcId){
			global $db;

			$sql = $db->prepare("Delete FROM PLC_Configuration where id=? ");
			$sql->execute(array($plcId));

			$sql = $db->prepare("Delete FROM ConfigurationDetail where PLC_id=? ");
			$sql->execute(array($plcId));

		}

		function copyTo($plcId, $newName){
			global $db;

			$sql = $db->prepare("INSERT into PLC_Configuration SET name=?, description=? ");
			$sql->execute(array($newName, $newName));

			$newId = $db->lastInsertId();

			$sql = $db->prepare("INSERT INTO ConfigurationDetail (PLC_id, `key`, `value`) SELECT  ".$newId.", `key`, `value` FROM ConfigurationDetail WHERE PLC_id =? ");
			$sql->execute(array($plcId));

			return $newId;
		}

		function getOldestPlcData() {
			global $db;

			$sql = $db->prepare("SELECT d.*, m.Tag_Name, c.name FROM PLC_Data as d, Register_Map as m, PLC_Configuration as c where d.Tx_Time = 0 AND d.Map_id=m.id And m.PLC_id=c.id order by d.Rx_Time desc limit 100");
			$sql->execute();
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $r; 
		}

		function getNewestPlcData() {
			global $db;

			$sql = $db->prepare("SELECT d.*, m.Tag_Name, c.name FROM PLC_Data as d, Register_Map as m, PLC_Configuration as c where d.Tx_Time = 0 AND d.Map_id=m.id And m.PLC_id=c.id order by d.Rx_Time asc limit 100");
			$sql->execute();
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $r; 
		}

		function getLatestSentData() {
			global $db;

			$sql = $db->prepare("SELECT d.*, m.Tag_Name, c.name FROM PLC_Data as d, Register_Map as m, PLC_Configuration as c where d.Tx_Time <> 0 AND d.Map_id=m.id And m.PLC_id=c.id order by d.Tx_Time asc limit 100 ");
			$sql->execute();
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $r; 
		}

	} //class ends
}//if

?>