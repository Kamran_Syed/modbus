<?php
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  require_once("config.php");
  require_once("plcModel.php");
	require_once("header.php");

  $m = new PlcModel();

  if(isset($_POST['delete'])){

    $m->delete($_POST['plc_id']);

  }elseif(isset($_POST['copyTo'])){

    $newId = $m->copyTo($_POST['plc_id'], $_POST['copyName']);
    header('Location: edit.php?plc_id='.$newId);
    exit;

  }elseif(isset($_POST['createNow'])){

    $newId = $m->createNewPlc($_POST['newName'], $_POST['newDesc']);
    header('Location: edit.php?plc_id='.$newId);
    exit;

  }

  $rows = $m->getAllPlc();

?>

<style>
  label {
    display: inline-block;
    margin-bottom: .5rem;
    padding-left: 2px;
  }

  h2 {
    font-weight: bold;
  }

  hr {
    margin-top: 135px;
  }
  .row{
    background-color: aliceblue;
    padding: 3px;
    margin-top: 5px;
  }
</style>
<script>
  function rowClick(e){
    jQuery('.btns').hide();
    jQuery('.plc').css('font-weight','normal');
    jQuery('.ctn').removeAttr('required');
    jQuery(e).next().show();
    jQuery(e).css('font-weight','bold');
  }

  function copyShow(id){
    jQuery('.copy-'+id).show();
    jQuery('.copy-'+id).attr('required', 'required');
  }

</script>
<div class="container">
  

    <div class="row">
       <div class="col-md-3" style="text-align: center;">
          <label>PLC Name</label>
       </div>
       <div class="col-md-3" style="text-align: center;">
         <label>Description</label>
       </div>
       <div class="col-md-6"></div>
    </div>
    <?php
      if($rows){
        foreach($rows as $r){

    ?>
    <div class="row plc" onclick="rowClick(this)">
      <div class="col-md-3">
         <?php echo $r['name'];?>
      </div>
      <div class="col-md-3">
         <?php echo $r['description'];?>
      </div>
      <div class="col-md-6"></div>
    </div>
    <div class="row btns" style="display: none;">
      <form method="post" action="">
        <input type="hidden"  name="plc_id" value="<?php echo $r['id'];?>" >
          <div class="col-md-1">
             <button type="button" name="edit" class="btn btn-primary" onclick="window.location.href='edit.php?plc_id=<?php echo $r['id'];?>'">Edit</button>
          </div>
          <div class="col-md-1">
             <a href="#" onclick="copyShow(<?php echo $r['id'];?>);" ><button type="button" name="copy-to" class="btn btn-primary">Copy To</button></a>
          </div>
          <div class="col-md-2">
             <button type="button" name="register-map" class="btn btn-primary" onclick="window.location.href='registerMap.php?plc_id=<?php echo $r['id'];?>'">Register Map</button>
          </div>
          <div class="col-md-1">
             <!--<a href='liveView.php?plc_id=<?php echo $r['id'];?>' ><button type="button" name="live-view" class="btn btn-primary" >Live View</button></a>-->
          </div>
          <div class="col-md-1">
            <button type="submit" name="delete" value='yes' class="btn btn-danger">Delete</button>
          </div>
          <div class="col-md-4"><input type="text" class="form-control btns ctn copy-<?php echo $r['id'];?>" style="display:none;" name="copyName" placeholder="Copy To Name" ></div>
          <div class="col-md-2"><button type="submit" name="copyTo" value='yes' class="btn btn-default btns copy-<?php echo $r['id'];?>"  style="display:none;">Copy Now</button></div>
      </form>
    </div>
    <?php
        }//foreach
      }//if rows
    ?>
    
      <hr>
<div class="container">
  <form method="post" action="">
      <div class="row form-group">
         <h2>New PLC</h2>
      </div>
      <div class="row form-group">
         <div class="col-sm-6" style="padding-left: 0px;">
            <input type="text" class="form-control" name="newName" placeholder="Name of PLC" required="required">
         </div>
      </div>
       <div class="row form-group">
         <div class="col-sm-6" style="padding-left: 0px;">
            <input type="text" class="form-control"  name="newDesc" placeholder="Description" required="required">
         </div>
      </div>
      <div class="row form-group" >
         <div class="col-sm-6" style="padding-left: 0px;" >
            <button type="submit" name="createNow" value='yes' class="btn btn-default">Create Now</button>
         </div>
      </div>
      
  </form>
</div>

<?php require_once 'footer.php';?>
