<?php

  require_once("config.php");
  require_once("plcModel.php");
	require_once("header.php");

  $m = new PlcModel();

  date_default_timezone_set('Australia/Perth');


?>

<style>
  label {
    display: inline-block;
    margin-bottom: .5rem;
    padding-left: 2px;
  }

  h2 {
    font-weight: bold;
  }

  hr {
    margin-top: 135px;
  }
  .row{
    background-color: aliceblue;
    padding: 3px;
    margin-top: 5px;
  }
</style>

<div class="container">
    <div class="row">
      <div class="col-sm-2"><a href="plcData.php?mode=oldest" >Waiting Oldest</a></div>
      <div class="col-sm-2"><a href="plcData.php?mode=newest" >Waiting Newest</a></div>
      <div class="col-sm-2"><a href="plcData.php?mode=lastsent" >Last Sent</a></div>
    </div>

    <?php
      $mode = $_GET['mode'];

      if(!isset($mode))  $rows = $m->getOldestPlcData();
      if($mode == 'oldest')  $rows = $m->getOldestPlcData();
      else if($mode == 'newest')  $rows = $m->getNewestPlcData();
      else if($mode == 'lastsent')  $rows = $m->getLatestSentData();

      ?>
      <div class="row">
        <div class="col-sm-12">
         <?php if(!isset($mode)) { ?><h2 style="text-align: center;">PLC Data (Oldest)</h2> <?php } ?>
         <?php if($mode == 'oldest') { ?><h2 style="text-align: center;">PLC Data (Waiting Oldest)</h2> <?php } 
         else if($mode == 'newest') { ?><h2 style="text-align: center;">PLC Data (Waiting Newest)</h2> <?php } 
         else if($mode == 'lastsent') { ?><h2 style="text-align: center;">PLC Data (Last Sent)</h2> <?php } ?>
        </div>
      </div>
      <?php

      if($rows){
        foreach($rows as $r){

            $cuurentTime = time();
            $waiting = $r['Rx_Time'];
            $waitingTime = $cuurentTime - $waiting;

    ?>
    <?php
      if ($mode == 'lastsent') { ?>  
          <div class="row">
            <div class="col-md-2"><?php echo date('d-m-Y h:i:s', $r['Rx_Time']);?></div>
            <div class="col-md-2"><?php echo date('d-m-Y h:i:s', $r['Tx_Time']);?></div>
            <div class="col-md-2"><?php echo $r['name'];?></div>
            <div class="col-md-2"><?php echo $r['Tag_Name'];?></div>
            <div class="col-md-2"><?php echo $r['D_Value'];?></div>
            <div class="col-md-2"></div>
          </div>
    <?php } else { ?>
          <div class="row">
            <div class="col-md-2"><?php echo date('d-m-Y h:i:s', $r['Rx_Time']);?></div>
            <div class="col-md-2"><?php echo $waitingTime;?></div>
            <div class="col-md-2"><?php echo $r['name'];?></div>
            <div class="col-md-2"><?php echo $r['Tag_Name'];?></div>
            <div class="col-md-2"><?php echo $r['D_Value'];?></div>
            <div class="col-md-2"></div>
          </div>
    <?php } ?>

    <?php
        }//each
      }//if rows
    ?>

<?php require_once 'footer.php';?>
