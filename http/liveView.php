<?php

  require_once("config.php");
  require_once("plcModel.php");
	require_once("header.php");

  $m = new PlcModel();

  date_default_timezone_set('Australia/Perth');

  $rows = $m->getLog();

?>

<style>
  label {
    display: inline-block;
    margin-bottom: .5rem;
    padding-left: 2px;
  }

  h2 {
    font-weight: bold;
  }

  hr {
    margin-top: 135px;
  }
  .row{
    background-color: aliceblue;
    padding: 3px;
    margin-top: 5px;
  }
</style>

<div class="container">
  	<div class="row">
      <div class="col-sm-12">
      	<h2 style="text-align: center;">Latest Logs</h2>
      </div>
    </div>

    <?php
      if($rows){
        foreach($rows as $r){

    ?>

    <div class="row">
      <div class="col-md-2">
         <?php echo date('d-m-Y h:i:s', $r['dt']);?>
      </div>
      <div class="col-md-1">
         <?php echo strtoupper($r['logType']);?>
      </div>
      <div class="col-md-6"><?php echo $r['msg'];?></div>
      <div class="col-md-3"></div>
    </div>

    

   

   
    <?php
        }//each
      }//if rows
    ?>

<?php require_once 'footer.php';?>
