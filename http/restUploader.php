<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once 'config.php';
require_once 'plcModel.php';

if(!class_exists('RestUploader')){
	class RestUploader{
				
		function __construct(){
			$m = new plcModel();

			try{
				$recs = $this->upload();
				$m->log("Uploaded {$recs} records");

			}catch(Exception $ex){
				
				$m->log("API Error: ". $ex->getMessage());
				exit;
			}
			

		}

		function upload(){

			$m = new plcModel();

			$rows = $m->getUploadData();
			if($rows){

				$plcId = 0;
				$endPoint = "NONE";
				$authKey = "NONE";
				$jd = new stdClass();
				$tm = 0;
				$readings = "";
				$uploadedIds = array();

				foreach($rows as $r){
					if($plcId != $r['PLC_id']){ //create new connection for new plc
						//upload data here
						if($readings != ""){
							$jd->data[] = $readings;
							$kv = array();
							$kv['uploadIds'] = $uploadedIds;
							$kv['jd'] = json_encode($jd);
							$kv['key'] = $authKey;
							$host = $endPoint;

							$this->sendPostRaw($host, $kv);

						}
						
						
						$jd = new stdClass();
						$uploadedIds = array();
						$tm = 0; //have done it so following if pics it up and reset readings
						$readings = "";

						$plcId = $r['PLC_id'];
						$endPoint = $m->getPlcConfig($plcId, 'api_endpoint');
						$authKey = $m->getPlcConfig($plcId, 'api_authkey');
						$jd->data = array();
					}

					if($tm != $r['Rx_Time']){ //send all readings together taken at same time
						$tm = $r['Rx_Time'];

						if($readings != "") $jd->data[] = $readings; //add previus readings set into data object

						$readings = new stdClass();
						$readings->dateTime = gmdate('Y-m-d\TH:i:s\Z', $tm);
					}


					$readings->{$r['Tag_Name']} = $r['D_Value'];
					$uploadedIds[] = $r['id'];

				}
				if($readings != ""){
					//upload data here
					$jd->data[] = $readings;
					
					$kv = array();
					$kv['uploadIds'] = $uploadedIds;
					$kv['jd'] = json_encode($jd);
					$kv['key'] = $authKey;
					$host = $endPoint;

					$this->sendPostRaw($host, $kv);
				} 
				

			}else{
				return 0;
			}
			
			return count($rows);
		}

		private function sendPostRaw($host, $kv){
			$m = new plcModel();
			$ch = curl_init();

			$headers  = [
	            'Authorization: '. $kv['key'],
	            'Content-Type: application/json'
	        ];

			curl_setopt($ch, CURLOPT_URL, $host);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $kv['jd']);
			//curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch,CURLOPT_FAILONERROR,true);

			$server_output = curl_exec ($ch);

			if ($server_output === FALSE) {
				
				$m->log("API Error: ". curl_error($ch));

			} else {
				$m->updateTx($kv['uploadIds']);
			}

			curl_close ($ch);

			

		}

		
	} //class ends
}//if

if(isset($argv[1])){
	$tm = intval($argv[1]);
	$obj = null;
	while(1){
		$obj = new RestUploader();
		unset($obj);
		$obj = null;
		sleep($tm);

	}
}else{
	new RestUploader();
}

?>