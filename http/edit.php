<?php
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  require_once("config.php");
  require_once("plcModel.php");
	require_once("header.php");

  $m = new PlcModel();

  if(! isset($_GET['plc_id'])){
    
    echo "<h1>Incorrect PLC</h1>";
    require_once 'footer.php';
    exit;
  }

  $plc = $m->getPlc($_GET['plc_id']);
  if(! $plc){
    echo "<h1>PLC not foundl</h1>";
    require_once 'footer.php';
    exit;
  }

  if(isset($_POST['plc_id'])){

    $m->updateConfig($_POST);

  }

  

?>

<style>
  label {
    display: inline-block;
    margin-bottom: .5rem;
    padding-left: 2px;
  }

  h2 {
    font-weight: bold;
  }

  hr {
    margin-top: 135px;
  }
  .row{
    background-color: aliceblue;
    padding: 5px;
    margin-top: 5px;
  }
  .txt{
    padding-top: 7------px;
  }
</style>

<div class="container"> 

  <div class="row">
    <div class="col-sm-12"><h3 style="text-align: center;">Configuration for <?php echo $plc['name'];?> </h3> </div>
  </div>

  <div class="row">
    <div class="col-sm-12"><h3 style="text-align: center;"><?php if(isset($_POST['plc_id'])&& $plc) echo "Data updated.";?> </h3> </div>
  </div>

    <div class="row">
       <div class="col-md-3" style="text-align: center;">
          <label>Items</label>
       </div>
       <div class="col-md-3" style="text-align: center;">
         <label>Values</label>
       </div>
       <div class="col-md-6"></div>
    </div>
    <?php
      $plcId = $plc['id'];
      $plc_ipaddress = $m->getPlcConfig($plcId, 'plc_ipaddress');
      $plc_port = $m->getPlcConfig($plcId, 'plc_port');
      $api_endpoint = $m->getPlcConfig($plcId, 'api_endpoint');
      $api_authkey = $m->getPlcConfig($plcId, 'api_authkey');

    ?>
  <form method="post" action="">

    <div class="row">
      <div class="col-md-3 txt">
         <?php echo strtoupper('plc_ipaddress');?>
      </div>
      <div class="col-md-6">
         <input type="text" class="form-control" name="plc_ipaddress" placeholder="IP Address of PLC" value="<?php echo $plc_ipaddress;?>" required />
      </div>
      <div class="col-md-3"></div>
    </div>

    <div class="row">
      <div class="col-md-3 txt">
         <?php echo strtoupper('plc_port');?>
      </div>
      <div class="col-md-6">
         <input type="text" class="form-control" name="plc_port" placeholder="TCP Port of PLC" value="<?php echo $plc_port;?>" required />
      </div>
      <div class="col-md-3"></div>
    </div>

    <div class="row">
      <div class="col-md-3 txt">
         <?php echo strtoupper('api_endpoint');?>
      </div>
      <div class="col-md-6">
         <input type="text" class="form-control" name="api_endpoint" placeholder="Rest service endpoint" value="<?php echo $api_endpoint;?>" required />
      </div>
      <div class="col-md-3"></div>
    </div>
    
    <div class="row">
      <div class="col-md-3 txt">
         <?php echo strtoupper('api_authkey');?>
      </div>
      <div class="col-md-6">
         <input type="text" class="form-control" name="api_authkey" placeholder="Authorization token/key" value="<?php echo $api_authkey;?>" required />
      </div>
      <div class="col-md-3"></div>
    </div>

    <div class="row">
      <div class="col-md-3 txt">
         <input type="hidden" name="plc_id" value="<?php echo $plcId;?>" />
      </div>
      <div class="col-md-6">
         <button type="submit" name="update" value='yes' class="btn btn-primary"  >Update Configuration</button>
      </div>
      <div class="col-md-3"></div>
    </div>

  </form>



    

<?php require_once 'footer.php';?>
