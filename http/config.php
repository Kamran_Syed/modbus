<?php
if(!class_exists('PlcDbConfig')){
	final class PlcDbConfig{
				
		public static function getMeDbConn(){
			$host = "localhost";
			$username = "pi";
			$password = "abc123";
			$dbname = "RPI3";
			
			// Create connection
			$db = new PDO('mysql:host='.$host.':3306;dbname='.$dbname.';charset=utf8mb4', $username, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
			
			return $db;
		}
		
	} //class ends
}//config ends

if(! session_id()) session_start();

$db = PlcDbConfig::getMeDbConn();
?>