<?php
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  
  
	require_once("config.php");
	require_once("registerMapModel.php");
	require_once("header.php"); 
	
  $plc_id = $_GET['plc_id'];
  if(! isset($plc_id)){
  	echo "Plc id not found";
  	exit;
  }

  $rmm = new RegisterMapModel();
?>
  <style>
	  label {
	    display: inline-block;
	    margin-bottom: .5rem;
	    padding-left: 2px;
	  }

	  h2 {
	    font-weight: bold;
	  }

	  hr {
	    margin-top: 135px;
	  }
	  .row{
	    background-color: aliceblue;
	    padding: 3px;
	    margin-top: 5px;
	  }
	</style>
	<script>
	  function rowClick(e){
	    jQuery('.btns').hide();
	    jQuery('.ctn').removeAttr('required');
	    jQuery(e).next().show();
	    jQuery(e).css('font-weight','bold');
	  }

	</script>
<?php

  if(isset($_POST['submit'])){
		$plc_id = $_GET['plc_id'];
	    $Tag_Name = $_POST['Tag_Name'];
		$Data_Type = $_POST['Data_Type'];
		$Tag_Decription = $_POST['Tag_Decription'];
		$Modbus_Address = trim($_POST['Modbus_Address']);
			
		// checking empty fields
		if(empty($Tag_Name) || empty($Data_Type) || empty($Tag_Decription) || empty($Modbus_Address)) {                
			if(empty($Tag_Name)) {
				echo "<font color='red'>Tag Name field is empty.</font><br/>";
			}
			
			if(empty($Data_Type)) {
				echo "<font color='red'>Please select data type.</font><br/>";
			}
			
			if(empty($Tag_Decription)) {
				echo "<font color='red'>Tag Description field is empty.</font><br/>";
			}
			
			 if(empty($Modbus_Address)) {
				echo "<font color='red'>Modbus Address field is empty.</font><br/>";
			}
			
		}
		else{
			
			$rmm->aspk_insert($plc_id,$Tag_Name,$Data_Type,$Tag_Decription,$Modbus_Address);
			
			
		}
  }
  elseif(isset($_POST['save'])){
  		$plc_id = $_GET['plc_id'];
	    $Tag_Name = $_POST['Tag_Name'];
		$Data_Type = $_POST['Data_Type'];
		$Tag_Decription = $_POST['Tag_Decription'];
		$Modbus_Address = $_POST['Modbus_Address'];

		$rmm->aspk_update($plc_id,$Tag_Name,$Data_Type,$Tag_Decription,$Modbus_Address);
		
  }
  
  elseif(isset($_POST['delete'])){

    $rmm->aspk_delete($_POST['plc_id']);

  } elseif(isset($_POST['edit'])){
		$plcDetail = $rmm->getPlc($_POST['plc_id']);
		$Tag_Name = $plcDetail['Tag_Name'];
		$Data_Type = $plcDetail['Data_Type'];
		$Tag_Decription = $plcDetail['Tag_Decription'];
		if($Data_Type != 'bool'){
			$Modbus_Address = intval($plcDetail['Modbus_Address']);
	
		}else{
			$Modbus_Address = $plcDetail['Modbus_Address'];
		}
		
	?>
<div class="container">
	<form method="post">
		<div class="row form-group">
			<h3>Modbus Map Configuration</h3>
		</div>
		<div class="row form-group">
			PLC id: <?php echo $plc_id;?> 
		</div>
		<div class="row form-group">
			<div class="col-sm-3" style="padding-left: 0px;">
				<label for="Tag_Name">Tag Name:</label>
				<input type="text" class="form-control" value="<?php echo $Tag_Name;?>" id="Tag_Name" placeholder="Enter Tag Name" name="Tag_Name" style="width: 20em;" required>
			</div>
		</div>
		<div class="row form-group">
			<div class="aspk_dropdown">
				<div class="col-sm-3" style="padding-left: 0px;">
					<label for="Data_Type">Data Type:</label>
					<br>
					<select name="Data_Type" id="dataType" value="<?php echo $Data_Type;?>" >
					  <option <?php if($Data_Type == "int") echo "selected" ?> value="int">INT</option>
					  <option <?php if($Data_Type == "dint") echo "selected" ?> value="dint">DINT</option>
					  <option <?php if($Data_Type == "real") echo "selected" ?> value="real">REAL</option>
					  <option <?php if($Data_Type == "bool") echo "selected" ?> value="bool">BOOL</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-sm-3" style="padding-left: 0px;">
				<label for="Tag_Decription">Tag Description:</label>
				<input type="text" class="form-control" id="Tag_Decription" value="<?php echo $Tag_Decription;?>" placeholder="Enter Tag Description" name="Tag_Decription" style="width: 20em;" required>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-sm-3" style="padding-left: 0px;">
				<label for="Modbus_Address">Modbus Address:</label>
				<input type="number" class="form-control" id="Modbus_Address" value="<?php echo $Modbus_Address;?>" placeholder="Enter Modbus Address" name="Modbus_Address" style="width: 20em;" required>
			</div>
		</div>
		<button type="submit" name="save" class="btn btn-default">Save</button>
	</form>
</div>
<?php exit();
  }
	  $rows = $rmm->getAllPlc($plc_id);

	?>
	<div class="container">
	    <div class="row">
	       <div class="col-md-3" style="text-align: center;">
	          <label>Tag Name</label>
	       </div>
	       <div class="col-md-3" style="text-align: center;">
	         <label>Data Type</label>
	       </div>
		   <div class="col-md-3" style="text-align: center;">
	         <label>Tag Description</label>
	       </div>
		   <div class="col-md-3" style="text-align: center;">
	         <label>Modbus Address</label>
	       </div>
	    </div>
	    <?php
	      if($rows){
	        foreach($rows as $r){
	        	if($r['Data_Type'] != 'bool'){
					$Modbus_Address = intval($r['Modbus_Address']);
			
				}else{
					$Modbus_Address = $r['Modbus_Address'];
				}

	    ?>
	    <div class="row map" onclick="rowClick(this)">
	      <div class="col-md-3">
	         <?php echo $r['Tag_Name'];?>
	      </div>
	      <div class="col-md-3">
	         <?php echo strtoupper($r['Data_Type']);?>
	      </div>
		  <div class="col-md-3">
	         <?php echo $r['Tag_Decription'];?>
	      </div>
	      <div class="col-md-3">
	         <?php echo $Modbus_Address;?>
	      </div>
		  
	     
	    </div>
	    <div class="row btns" style="display: none;">
	      <form method="post" action="">
	        <input type="hidden"  name="plc_id" value="<?php echo $r['id'];?>" >
	          <div class="col-md-1">
	             <button type="submit" name="edit" class="btn btn-primary" onclick="window.location.href='registerMap.php?plc_id=<?php echo $r['id'];?>'">Edit</button>
	          </div>
	          <div class="col-md-1">
	            <button type="submit" name="delete" value='yes' class="btn btn-danger">Delete</button>
	          </div>
			  <div class="col-md-10"></div>
	      </form>
	    </div>
	    <?php
	        }//foreach
	      }//if rows
	    ?>
	    
	      <hr>

	<div class="container">
		<form method="post" action="">
			<div class="row form-group">
				<h3>Modbus Map Configuration</h3>
			</div>
			<div class="row form-group">
				PLC id: <?php echo $plc_id;?> 
			</div>
			<div class="row form-group">
				<div class="col-sm-3" style="padding-left: 0px;">
					<label for="Tag_Name">Tag Name:</label>
					<input type="text" class="form-control" id="Tag_Name" placeholder="Enter Tag Name" name="Tag_Name" style="width: 20em;">
				</div>
			</div>
			<div class="row form-group">
				<div class="aspk_dropdown">
					<div class="col-sm-3" style="padding-left: 0px;">
						<label for="Data_Type">Data Type:</label>
						<br>
						<select name="Data_Type">
						  <option value="int">INT</option>
						  <option value="dint">DINT</option>
						  <option value="real">REAL</option>
						  <option value="bool">BOOL</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-3" style="padding-left: 0px;">
					<label for="Tag_Decription">Tag Description:</label>
					<input type="text" class="form-control" id="Tag_Decription" placeholder="Enter Tag Description" name="Tag_Decription" style="width: 20em;">
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-3" style="padding-left: 0px;">
					<label for="Modbus_Address">Modbus Address:</label>
					<input type="number" class="form-control" id="Modbus_Address" placeholder="Enter Modbus Address" name="Modbus_Address" style="width: 20em;">
				</div>
			</div>
			<button type="submit" name="submit" class="btn btn-default">Submit</button>
		</form>
	</div>  
<?php require_once 'footer.php';?>
