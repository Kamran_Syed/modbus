<?php
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  require_once("config.php");
  require_once("plcModel.php");

  $m = new PlcModel();

  date_default_timezone_set('Australia/Perth');

  $rows = $m->getRegisterMap();

  if($rows){
  	
  	$dmy = new stdClass();
  	$dmy->metadata = array();
  	$dr = array();

  	foreach ($rows as $r) {

  		$cr = new stdClass();
  		$cr->TagName = $r['Tag_Name'];
  		$cr->Description = $r['Tag_Decription'];
  		$srcdt = $r['Data_Type'];
  		$tgtdt = "float";

  		switch ($srcdt) {
  			case 'bool':
  				$tgtdt = "Int1";
  				break;
  			case 'int':
  				$tgtdt = "Int4";
  				break;
  			case 'dint':
  				$tgtdt = "UInt4";
  				break;
  			case 'real':
  				$tgtdt = "Double";
  				break;

  			default:
  				$tgtdt = "Double";
  				break;
  		}


  		$cr->DataType = $tgtdt;
  		$dr[] = $cr;


  	}

  	header('Content-disposition: attachment; filename=Metadata.json');
  	header('Content-Type: application/json');
  	header('Pragma: no-cache');
  	
  	$dmy->metadata = $dr;
  	echo json_encode($dmy);



  }else{
  	echo "Sorry, no data available for register map";

  }