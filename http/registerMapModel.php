<?php

if(!class_exists('RegisterMapModel')){
	class RegisterMapModel{
				
		function __construct(){

		}

		function aspk_insert($plc_id,$Tag_Name,$Data_Type,$Tag_Decription,$Modbus_Address){
			global $db;
			
			$sql= $db->prepare("INSERT INTO Register_Map SET PLC_id=?, Tag_Name=?,Data_Type=?,Tag_Decription=?,Modbus_Address=? ");
			$sql->execute(array($plc_id,$Tag_Name,$Data_Type,$Tag_Decription,$Modbus_Address));
			
		}
		
		function aspk_delete($id){
			global $db;
			$sql= $db->prepare("DELETE FROM Register_Map WHERE id=$id");
			$sql->execute();
			
		}
		
		function aspk_update($plc_id,$Tag_Name,$Data_Type,$Tag_Decription,$Modbus_Address){
			global $db;
			
			$sql= $db->prepare("UPDATE Register_Map SET Tag_Name=?,Data_Type=?,Tag_Decription=?,Modbus_Address=? WHERE id=$plc_id");
			$sql->execute(array($Tag_Name,$Data_Type,$Tag_Decription,$Modbus_Address));
		}

		function getAllPlc($plc_id){
			global $db;

			$sql = $db->prepare("SELECT * FROM Register_Map where PLC_id=? ");
			$sql->execute(array($plc_id));
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			
			return $r; //will be false if not found

		}

		function getPlc($plc_id) {
			global $db;

			$sql = $db->prepare("SELECT * FROM Register_Map WHERE id=$plc_id");
			$sql->execute();
			$r = $sql->fetch(PDO::FETCH_ASSOC);
			
			return $r; //will be false if not found
		}

		
	} //class ends
}//if

?>