#!/usr/bin/env python
import pymysql.cursors
import time

def getConnection():
    # Connect to the database
    connection = pymysql.connect(host='localhost',
                            port=3306, 
                             user='pi',
                             password='abc123',
                             db='RPI3',
                             autocommit='yes', 
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

    return connection
    
def getAllPlc(cn):
    with cn.cursor() as cursor:
        sql = "SELECT * FROM PLC_Configuration;"
        cursor.execute(sql)
        result = cursor.fetchall()
    return result
        
def getPlcConfig(cn,  plcId,  cKey):
    with cn.cursor() as cursor:
        sql = "SELECT * FROM ConfigurationDetail where `PLC_id`=%s AND `key`=%s;"
        cursor.execute(sql, (plcId,  cKey))
        result = cursor.fetchone()
    return result
    
def plcLog(cn, msg):
    with cn.cursor() as cursor:
        # Create a new record
        sql = "INSERT INTO PLC_logs (dt, logType, msg) VALUES (%s, %s, %s)"
        cursor.execute(sql, (int(time.time()), "PLC",  msg))
    return

def getRegisterMap(cn,  plcId):
    with cn.cursor() as cursor:
        sql = "SELECT * FROM Register_Map where `PLC_id`=%s;"
        cursor.execute(sql, (plcId))
        result = cursor.fetchall()
    return result

def saveData(cn, mapId,  v):
    with cn.cursor() as cursor:
        sql = "INSERT INTO PLC_Data (Map_id, Rx_Time, D_Value) VALUES (%s, %s, %s)"
        cursor.execute(sql, (mapId,  int(time.time()), v))
    return
