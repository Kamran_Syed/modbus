#!/usr/bin/env python

#---------------------------------------------------------------------------# 
# import the various server implementations
#---------------------------------------------------------------------------# 
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.exceptions import ConnectionException
import plcdb
import plcutil
import sys
import time

#---------------------------------------------------------------------------# 
# configure the client logging
#---------------------------------------------------------------------------# 
import logging
logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.DEBUG)
logging.disable(sys.maxsize)

cn = plcdb.getConnection()
rtm = 0
try:
    rtm = int(sys.argv[1])
    
except IndexError:
    rtm = 0

while True:
    try:
        result = plcdb.getAllPlc(cn)
        for r in result:
            plcId = r['id']
            plcHost = plcdb.getPlcConfig(cn,  plcId, 'plc_ipaddress')
            plcPort = plcdb.getPlcConfig(cn,  plcId, 'plc_port')
            
            client = ModbusClient(plcHost['value'], port=plcPort['value'], timeout=30)
            client.connect()

            rmap = plcdb.getRegisterMap(cn,  plcId)
            for mr in rmap:
                addr = mr['Modbus_Address']
                holdingReg = int(addr) - 40001
                frac = plcutil.getFrac(addr)
                
                dtype = mr['Data_Type']
                dtype = dtype.lower()
                mapId = mr['id']
                
                if dtype == 'int':
                    rr = client.read_holding_registers(holdingReg, 1, unit=0)
                    if not hasattr(rr, 'registers'):
                        plcdb.plcLog(cn,  "Unable to read from client")
                        continue

                    decoder = BinaryPayloadDecoder.fromRegisters(rr.registers, endian=Endian.Big)
                    num = decoder.decode_16bit_int()
                elif dtype == 'dint':
                    rr = client.read_holding_registers(holdingReg, 2, unit=0)
                    if not hasattr(rr, 'registers'):
                        plcdb.plcLog(cn,  "Unable to read from client")
                        continue
                     
                    decoder = BinaryPayloadDecoder.fromRegisters(rr.registers, endian=Endian.Big)
                    num = decoder.decode_32bit_int()
                elif dtype == 'real':
                    rr = client.read_holding_registers(holdingReg, 2, unit=0)
                    if not hasattr(rr, 'registers'):
                        plcdb.plcLog(cn,  "Unable to read from client")
                        continue
                     
                    decoder = BinaryPayloadDecoder.fromRegisters(rr.registers, endian=Endian.Big)
                    num = decoder.decode_32bit_float()
                    num = round(num,  12)
                elif dtype == 'bool':
                    rr = client.read_holding_registers(holdingReg, 1, unit=0)
                    if not hasattr(rr, 'registers'):
                        plcdb.plcLog(cn,  "Unable to read from client")
                        continue
                     
                    decoder = BinaryPayloadDecoder.fromRegisters(rr.registers, endian=Endian.Big)
                    num = decoder.decode_16bit_int()
                    num = num >> frac & 1
                else:
                    raise Exception('Data Type specified in map is invalid')
                
                plcdb.saveData(cn, mapId,  num)

            client.close()

        if rtm == 0:
            break

        time.sleep(rtm)
        
    except IOError as e:
        errno, strerror = e.args
        plcdb.plcLog(cn,  strerror)
        sys.exit()
    except ValueError:
        plcdb.plcLog(cn,  "Data Type conversion error")
        sys.exit()
    except ConnectionException as msg:
        plcdb.plcLog(cn,  "Connection Error:"+ str(msg))
        sys.exit()
     
cn.close()
